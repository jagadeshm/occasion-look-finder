import React, { Component } from 'react';
import data from './data/inputvalues.js';
import { assignIn } from 'lodash';

class App extends Component {
  constructor(){
    super();
    this.state={
      currentOccuationIndex: 0,
      currentLookIndex:0,
      currentImageIndex:0
    }
  }

  currentImageRender(i){
    console.log(i);
    this.setState({currentImageIndex:i});
  }

  renderImages(){
    const LookImages =data[this.state.currentOccuationIndex].looks[this.state.currentLookIndex].lookImage;
    // console.log(LookImages[this.state.currentImageIndex]);
    // console.log(LookImages[0].lookImage[0].lookImageUrl);
    // console.log(LookImages[0].lookImage[1].lookImageUrl);
    const css={
      mainImgStyle:{
        width:'70%',
        display:'block',
        float:'left',
        padding:'4% 1% 0 1%'
      },
      tileImgWrapperStyle:{
        width:'30%',
        display:'block',
        float:'left',
        paddingTop:'3%'
      }
    }

    const LookImagesDisplay = LookImages.map((val, i)=>{
      // console.log(i, val);

      const titleImgStyle={
          padding: '3%',
      }

      if(this.state.currentImageIndex === i){
        // titleStyle.backgroundColor= 'yellow';
        assignIn(titleImgStyle, { opacity:0.5 });
      } else {
        assignIn(titleImgStyle, { opacity:1 });
      }

    return (
        <div style={titleImgStyle} key={i}>
         <img onClick={this.currentImageRender.bind(this, i)} src={val.lookImageUrl} alt="Looks" />
        </div>
      );
    })

    return (
      <div>
        <a target="_blank" style={css.mainImgStyle} href={LookImages[this.state.currentImageIndex].lookImageLink}>
         <img src={LookImages[this.state.currentImageIndex].lookImageUrl} alt="Looks" />
        </a>
        <div style={css.tileImgWrapperStyle}>{LookImagesDisplay}</div>
      </div>
    );
  }


  handleClickLooks(i){
    if(i!== this.state.currentLookIndex){
      this.setState({ 
        currentLookIndex: i,
        currentImageIndex:0
      });
    }
    // console.log(i);
  }

  renderLooks(){
    // console.log(this.state.currentOccuationIndex);
    const LookData =data[this.state.currentOccuationIndex].looks;
    const LookTitle = LookData.map((val, i)=>{
      const { lookTitle } = val;
      // var LookImgUrl = val;
      // console.log(lookImage[0].lookImageUrl);
      const styleLookTitle={
        width:'31%',
        float:'left',
        color:'#fff',
        backgroundColor: "#ff8ddb",
        textAlign: 'center',
        padding: '2% 0',
        margin: '5% 1% 0'
      }


      if(this.state.currentLookIndex === i){
        // titleStyle.backgroundColor= 'yellow';
        assignIn(styleLookTitle, { backgroundColor: 'red', color: 'black' });
      } else {
        assignIn(styleLookTitle, { backgroundColor: '#ff8ddb' });
      }

      // console.log(data[this.state.currentOccuationIndex].looks[i].lookImage[i]);
      
      return (
        <div key={i}>
          <div onClick={this.handleClickLooks.bind(this, i)} style={styleLookTitle}>
            {lookTitle}
          </div>
          {this.renderImages.bind(this, val)}
        </div>
      );
    })
    return LookTitle;
  }

  handleClickTitle(i){
    // console.log('Clicked',i);
    if(i!== this.state.currentOccuationIndex){
      this.setState({
        currentOccuationIndex: i,
        currentLookIndex:0,
        currentImageIndex:0
      });
    }
  }

  renderMainTitles(){
    const mainTitle =data.map((val, i) => {
      const titleStyle={
        width:'31%',
        float:'left',
        color:'#fff',
        backgroundColor: "#968b8b",
        textTransform: 'uppercase',
        textAlign: 'center',
        padding: '2% 0',
        margin: '0 1%'
      }
      if(this.state.currentOccuationIndex === i){
        // titleStyle.backgroundColor= 'yellow';
        assignIn(titleStyle, { backgroundColor: 'yellow', color: 'black' });
      } else {
        assignIn(titleStyle, { backgroundColor: '#968b8b' });
      }

      const {mainTitle} = val;
      return (
        <div key={i} onClick={this.handleClickTitle.bind(this, i)} style={titleStyle}>
          {mainTitle}
        </div>
      );
    });
    return mainTitle;
  }

  render() {
    // console.log(data);
    return (
      <div>
        <h3>Occastion Finder</h3>
        {this.renderMainTitles()}
        {this.renderLooks()}
        {this.renderImages()}
      </div>
    );
  }
}

export default App;
